import java.util.List;

public class TwoSum {

    /*
        You are given a list of numbers, and a target number k. Return whether or not there are two numbers in the list that add up to k.

        Example:
        Given [4, 7, 1 , -3, 2] and k = 5,
        return true since 4 + 1 = 5.
     */

    private TwoSum(){
        // utility class
    }

    public static boolean isNumberSumOfTwoElements(List<Integer> inputList, int inputNumber){
        for(int i = 0; i < inputList.size(); i++){
            for(int j = i+1; j < inputList.size(); j++){
                int intToCompareTo = inputList.get(i);
                int currentInt = inputList.get(j);
                if(intToCompareTo + currentInt == inputNumber){
                    return true;
                }
            }
        }
        return false;
    }
}
