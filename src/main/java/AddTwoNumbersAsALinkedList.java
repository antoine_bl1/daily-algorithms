import java.util.LinkedList;

public class AddTwoNumbersAsALinkedList {

    /*
        Given two linked-lists representing two non-negative integers.
        The digits are store in reverse order and each of their nodes contain a single digit.
        Add the two numbers and return it as a linked list.
        ex :
        Input : (2 -> 4 -> 3) + (5 -> 6 -> 4)
        Output : 7 -> 0 -> 8
        Explaination : 342 + 465 = 807
     */

    private AddTwoNumbersAsALinkedList() {
        // utility class
    }

    public static LinkedList<Integer> getTwoNumberAsLinkedListSummed(LinkedList<Integer> l1, LinkedList<Integer> l2) {
        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        StringBuilder number1AsString = new StringBuilder();
        StringBuilder number2AsString = new StringBuilder();
        int maxListSize = Math.max(l1.size(), l2.size());
        for (int i = maxListSize; i >= 0; i--) {
            if (i < l1.size()) {
                number1AsString.append(l1.get(i));
            }
            if (i < l2.size()) {
                number2AsString.append(l2.get(i));
            }
        }
        Integer number1 = Integer.parseInt(number1AsString.toString());
        Integer number2 = Integer.parseInt(number2AsString.toString());
        String sumStr = Integer.toString(number1 + number2);
        for (int i = sumStr.length(); i > 0; i--) {
            linkedList.add(Integer.parseInt(String.valueOf(sumStr.charAt(i - 1))));
        }
        return linkedList;
    }
}
