public class BuddyStrings {

    /*
        Given two strings A and B of lowercase letters, return true if and only if we can swap two letters in A so that the result equals B.

        exemples :
        areStringsBuddies("ab", "ba") -> true
        areStringsBuddies("ab", "ab") -> false
        areStringsBuddies("aa", "aa") -> true
        areStringsBuddies("aaaaaaabc", "aaaaaaacb") -> true
     */

    private BuddyStrings() {
        // utility class
    }

    public static boolean areStringsBuddies(String a, String b) {
        if (a.length() != b.length()) {
            return false;
        }
        int length = a.length();
        boolean stringContainTwoTimeTheSameChar = false;
        for (int i = 0; i < length; i++) {
            char ca1 = a.charAt(i);
            char cb1 = b.charAt(i);
            stringContainTwoTimeTheSameChar = a.chars().filter(c -> c == ca1).count() > 1;
            if (ca1 != cb1 && i < length + 1) {
                for (int j = i + 1; j < length; j++) {
                    char ca2 = a.charAt(j);
                    if (ca2 == cb1) {
                        // we make the character changing
                        char[] aCharacters = a.toCharArray();
                        aCharacters[i] = ca2;
                        aCharacters[j] = ca1;
                        String aChanged = new String(aCharacters);
                        return aChanged.equals(b);
                    }
                }
            }
        }
        // in this case, all charaactes were equals, between both strings
        // but if the char is present two times, then we can switch their positions and still have the same result
        return stringContainTwoTimeTheSameChar;
    }
}
