import com.google.common.collect.Lists;

import java.util.List;

public class SortingAListWith3UniqueNumbers {

    /*
        Given a list of numbers with only 3 unique numbers (1, 2, 3), sort the list in O(n) time.

        Example 1:
        Input: [3, 3, 2, 1, 3, 2, 1]
        Output: [1, 1, 2, 2, 3, 3, 3]
     */
    private SortingAListWith3UniqueNumbers(){
        // utility class
    }

    public static List<Integer> getListSorted(List<Integer> inputList){
        List<Integer> sortedList = Lists.newArrayList(inputList);
        boolean sorted = false;
        while(!sorted){
            sorted = true;
            for(int i = 0; i < sortedList.size() - 1; i ++){
                int currentValue = sortedList.get(i);
                int nextValue = sortedList.get(i + 1);
                if(currentValue > nextValue){
                    sortedList.set(i, nextValue);
                    sortedList.set(i + 1, currentValue);
                    // if a switch between two number has been set
                    // it mean the iteration hasn't been threw without any change -> it might not been sorted then
                    sorted = false;
                }
            }
        }
        return sortedList;
    }
}
