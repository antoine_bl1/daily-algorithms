package model;

public class Node<T>{

    private T value;
    private Node<T> left;
    private Node<T> right;

    public Node(T value){
        this.value = value;
        this.left = null;
        this.right = null;
    }

    public T getValue(){
        return this.value;
    }

    public void setValue(T value){
        this.value = value;
    }

    public Node<T> getLeft(){
        return this.left;
    }

    public void setLeft(Node<T> left){
        this.left = left;
    }

    public Node<T> getRight(){
        return this.right;
    }

    public void setRight(Node<T> right){
        this.right = right;
    }

    @Override
    public String toString() {
        return toString(this);
    }

    private String toString(Node<T> root) {
        String result = "";
        if(root == null) {
            result = "null";
        }
        else {
            result = result + root.getValue();
            if(root.getLeft() != null || root.getRight() != null){
                result = "(" + result + ", " + toString(root.getLeft());
                result = result + ", " + toString(root.getRight()) + ")";
            }
        }
        return result;
    }
}