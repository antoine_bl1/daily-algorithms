import java.util.Arrays;

public class EditDistance {

    /*
        Given two strings, determine the edit distance between them.
        The edit distance is defined as the minimum number of edits (insertion, deletion, or substitution) needed to change one string to the other.

        For example, "biting" and "sitting" have an edit distance of 2 (substitute b for s, and insert a t).
        getEditDistance("casse", "classe") -> 1
     */

    private EditDistance(){
        // utility class
    }

    public static int getEditDistance(String x, String y) {
        if (x.isEmpty()) {
            return y.length();
        }
        if (y.isEmpty()) {
            return x.length();
        }
        int substitution = getEditDistance(x.substring(1), y.substring(1)) + costOfSubstitution(x.charAt(0), y.charAt(0));
        int insertion = getEditDistance(x, y.substring(1)) + 1;
        int deletion = getEditDistance(x.substring(1), y) + 1;
        return min(substitution, insertion, deletion);
    }

    public static int costOfSubstitution(char a, char b) {
        return a == b ? 0 : 1;
    }

    public static int min(int... numbers) {
        return Arrays.stream(numbers).min().orElse(Integer.MAX_VALUE);
    }
}
