import com.google.common.collect.Sets;

import java.util.Set;

public class LongestSubstringWithoutRepeating {

    /*
        Given a string, find the length of the longest substring without repeating characters.
        ex : "abrkaabcdefghijjxxx" -> 10
     */

    private LongestSubstringWithoutRepeating() {
        // utility class
    }

    public static int getLengthOfLongestSubstringWithoutRepeating(String str) {
        Set<Character> substringSetCharacters = Sets.newHashSet();
        int maxLength = 0;
        int currentLength = 0;
        for (int i = 0; i < str.length(); i++) {
            char currentCharacter = str.charAt(i);
            if (!substringSetCharacters.contains(currentCharacter)) {
                substringSetCharacters.add(currentCharacter);
                currentLength++;
                if (currentLength > maxLength) {
                    maxLength = currentLength;
                }
            } else {
                substringSetCharacters.clear();
                substringSetCharacters.add(currentCharacter);
                currentLength = 1;
            }
        }
        return maxLength;
    }
}