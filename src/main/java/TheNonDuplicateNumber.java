import java.util.List;

public class TheNonDuplicateNumber {

    /*
        Given a list of numbers, where every number shows up twice except for one number, find that one number.

        Example:
        Input: [4, 3, 2, 4, 1, 3, 2]
        Output: 1
     */
    private TheNonDuplicateNumber(){
        // utility class
    }

    public static Integer getTheNonDuplicateNumber(List<Integer> inputNumbersList){
        for(int i = 0; i < inputNumbersList.size(); i++){
            int valueToCompareTo = inputNumbersList.get(i);
            for(int j = 0; j < inputNumbersList.size(); j++){
                int valueCompared = inputNumbersList.get(j);
                if(i != j && valueToCompareTo == valueCompared){
                    break;
                }else if(j == inputNumbersList.size() - 1){
                    return valueToCompareTo;
                }
            }
        }
        return null;
    }
}
