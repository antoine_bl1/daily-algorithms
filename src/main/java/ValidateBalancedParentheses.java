import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.LinkedList;
import java.util.Map;

public class ValidateBalancedParentheses {
    /**
     * Given a string containing just the characters '(', ')', '{', '}', '[' and ']',
     * determine if the input string is valid.
     * An input string is valid if:
     * - Open brackets are closed by the same type of brackets.
     * - Open brackets are closed in the correct order.
     * - Note that an empty string is also considered valid.
     *
     * Example:
     * Input: "((()))"
     * Output: True
     *
     * Input: "[()]{}"
     * Output: True
     *
     * Input: "({[)]"
     * Output: False
     */

    private ValidateBalancedParentheses() {
        // utility class
    }

    public static boolean areParenthesesBalanced(String str){
        Map<Character, Character> openingParenthesesByOpeningOne = Maps.newHashMap();
        openingParenthesesByOpeningOne.put('}', '{');
        openingParenthesesByOpeningOne.put(']', '[');
        openingParenthesesByOpeningOne.put(')', '(');
        LinkedList<Character> parentheses = Lists.newLinkedList();
        int strLength = str.length();
        for(int i = 0; i < strLength; i++){
            Character currentCharacter = str.charAt(i);
            // it's an openging parenthese
            if(openingParenthesesByOpeningOne.containsValue(currentCharacter)){
                parentheses.add(currentCharacter);
                continue;
            }
            Character corespondingOpeningParenthese = openingParenthesesByOpeningOne.get(currentCharacter);
            Character lastOpeningParenthese = parentheses.getLast();
            if(lastOpeningParenthese.equals(corespondingOpeningParenthese)){
                parentheses.removeLast();
            }else{
                return false;
            }
        }
        return true;
    }
}
