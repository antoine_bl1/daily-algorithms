import model.Node;

public class InvertABinaryTree {

    /*
        You are given the root of a binary tree. Invert the binary tree in place.
        That is, all left children should become right children, and all right children should become left children.

            a
           / \
          b   c
         / \  /
        d   e f

          a
         / \
         c  b
         \  / \
         f e   d
     */

    private InvertABinaryTree(){
        // utility class
    }

    public static Node<Character> getBinaryTreeInverted(Node<Character> binaryTree){
        reverseBinaryTree(binaryTree);
        return binaryTree;
    }

    private static void reverseBinaryTree(Node<Character> node){
        if(node != null){
            Node<Character> tempLeft = node.getLeft();
            node.setLeft(node.getRight());
            node.setRight(tempLeft);
            reverseBinaryTree(node.getLeft());
            reverseBinaryTree(node.getRight());
        }
    }
}
