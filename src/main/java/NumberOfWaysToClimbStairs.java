public class NumberOfWaysToClimbStairs {

    /*
        You are given a positive integer N which represents the number of steps in a staircase.
        You can either climb 1 or 2 steps at a time.
        Write a function that returns the number of unique ways to climb the stairs.

        NumberOfWaysToClimbStairs(4) -> 5
        NumberOfWaysToClimbStairs(5) -> 8
     */
    private NumberOfWaysToClimbStairs(){
        // utility class
    }

    public static int getNumberOfWaysToClimbStairs(int numberOfStairs){
        return fib(numberOfStairs + 1);
    }

    private static int fib(int n){
        if (n <= 1){
            return n;
        }
        return fib(n - 1) + fib(n - 2);
    }
}
