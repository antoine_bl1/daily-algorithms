import java.util.List;

public class FindPythagoreanTriplets {

    /*
        Given a list of numbers, find if there exists a pythagorean triplet in that list.
        A pythagorean triplet is 3 variables a, b, c where a2 + b2 = c2

        isPythagoreanTripletsPresent([3, 5, 12, 5, 13, 14]) -> true (52 + 122 = 132)
     */
    private FindPythagoreanTriplets(){
        // utility class
    }

    public static boolean isPythagoreanTripletsPresent(List<Integer> numbers){
        for(int i = 0; i < numbers.size() && numbers.size() >= 3; i++){
            int hypotenuse = numbers.get(i);
            for(int j = 0; j < numbers.size(); j++){
                if(j == i){
                    continue;
                }
                int a = numbers.get(j);
                for(int k = 0; k < numbers.size(); k++){
                    if(k == i || k == j){
                        continue;
                    }
                    int b = numbers.get(k);
                    if(Math.pow(hypotenuse, 2) == Math.pow(a, 2) + Math.pow(b, 2)){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
