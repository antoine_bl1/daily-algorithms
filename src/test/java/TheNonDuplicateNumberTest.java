import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class TheNonDuplicateNumberTest {

    @Test
    public void test(){
        Integer expectedResult = 1;
        Assert.assertEquals(expectedResult, TheNonDuplicateNumber.getTheNonDuplicateNumber(Arrays.asList(4, 3, 2, 4, 1, 3, 2)));
    }
}