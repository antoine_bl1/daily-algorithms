import org.junit.Assert;
import org.junit.Test;

public class BuddyStringsTest {

    @Test
    public void test1() {
        Assert.assertTrue(BuddyStrings.areStringsBuddies("ab", "ba"));
    }

    @Test
    public void test2() {
        Assert.assertFalse(BuddyStrings.areStringsBuddies("ab", "ab"));
    }

    @Test
    public void test3() {
        Assert.assertTrue(BuddyStrings.areStringsBuddies("aa", "aa"));
    }

    @Test
    public void test4() {
        Assert.assertTrue(BuddyStrings.areStringsBuddies("aaaaaaabc", "aaaaaaacb"));
    }

    @Test
    public void test5() {
        Assert.assertFalse(BuddyStrings.areStringsBuddies("", "b"));
    }
}