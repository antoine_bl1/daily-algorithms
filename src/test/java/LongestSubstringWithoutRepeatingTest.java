import org.junit.Assert;
import org.junit.Test;

public class LongestSubstringWithoutRepeatingTest {

    @Test
    public void testShould() {
        Assert.assertEquals(10, LongestSubstringWithoutRepeating.getLengthOfLongestSubstringWithoutRepeating("abrkaabcdefghijjxxx"));
        Assert.assertEquals(1, LongestSubstringWithoutRepeating.getLengthOfLongestSubstringWithoutRepeating("aaaaaaaaaa"));
        Assert.assertEquals(4, LongestSubstringWithoutRepeating.getLengthOfLongestSubstringWithoutRepeating("bblin"));
        Assert.assertEquals(5, LongestSubstringWithoutRepeating.getLengthOfLongestSubstringWithoutRepeating("abcdddefgh"));
    }
}