import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class FindPythagoreanTripletsTest {

    @Test
    public void test(){
        Assert.assertTrue(FindPythagoreanTriplets.isPythagoreanTripletsPresent(Arrays.asList(3, 5, 12, 5, 13)));
    }
}