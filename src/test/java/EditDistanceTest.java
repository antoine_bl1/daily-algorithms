import org.junit.Assert;
import org.junit.Test;

public class EditDistanceTest {

    @Test
    public void test(){
        Assert.assertEquals(2, EditDistance.getEditDistance("biting", "sitting"));
    }
}