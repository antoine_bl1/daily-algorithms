import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class TwoSumTest {

    @Test
    public void test1(){
        List<Integer> inputList = Lists.newArrayList(4, 7, 1 , -3, 2);
        int inputNumber = 5;
        Assert.assertTrue(TwoSum.isNumberSumOfTwoElements(inputList, inputNumber));
    }

    @Test
    public void test2(){
        List<Integer> inputList = Lists.newArrayList(0);
        int inputNumber = 4;
        Assert.assertTrue(TwoSum.isNumberSumOfTwoElements(inputList, inputNumber));
    }
}