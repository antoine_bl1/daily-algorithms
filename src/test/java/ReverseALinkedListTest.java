import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;

public class ReverseALinkedListTest {

    @Test
    public void test(){
        LinkedList<Integer> inputList = Lists.newLinkedList();
        inputList.add(4);
        inputList.add(3);
        inputList.add(2);
        inputList.add(1);
        inputList.add(0);
        LinkedList<Integer> expectedReversedList = Lists.newLinkedList();
        expectedReversedList.add(0);
        expectedReversedList.add(1);
        expectedReversedList.add(2);
        expectedReversedList.add(3);
        expectedReversedList.add(4);
        Assert.assertEquals(expectedReversedList, ReverseALinkedList.getReverseLinkedList(inputList));
    }
}