import model.Node;
import org.junit.Assert;
import org.junit.Test;

public class InvertABinaryTreeTest {

    /*
            a
           / \
          b   c
         / \  /
        d   e f

          a
         / \
        c    b
         \  / \
         f e   d
     */

    @Test
    public void test(){
        Node<Character> root = new Node<Character>('a');
        root.setLeft(new Node<Character>('b'));
        root.setRight(new Node<Character>('c'));
        root.getLeft().setLeft(new Node<Character>('d'));
        root.getLeft().setRight(new Node<Character>('e'));
        root.getRight().setLeft(new Node<Character>('f'));
        Node<Character> expectedReveresedBinaryTree = new Node<Character>('a');
        expectedReveresedBinaryTree.setLeft(new Node<Character>('c'));
        expectedReveresedBinaryTree.setRight(new Node<Character>('b'));
        expectedReveresedBinaryTree.getLeft().setRight(new Node<Character>('f'));
        expectedReveresedBinaryTree.getRight().setLeft(new Node<Character>('e'));
        expectedReveresedBinaryTree.getRight().setRight(new Node<Character>('d'));
        Assert.assertEquals(expectedReveresedBinaryTree.toString(), InvertABinaryTree.getBinaryTreeInverted(root).toString());
    }
}