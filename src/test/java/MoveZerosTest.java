import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class MoveZerosTest {

    @Test
    public void test() {
        List<Integer> inputNumbers = Arrays.asList(0, 1, 0, 3, 12);
        List<Integer> expectedNumbersWithZerosAtTheEnd = Arrays.asList(1, 3, 12, 0, 0);
        Assert.assertEquals(expectedNumbersWithZerosAtTheEnd, MoveZeros.moveZeros(inputNumbers));
    }
}