import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;

public class AddTwoNumbersAsALinkedListTest {

    @Test
    public void test() {
        LinkedList<Integer> expectedLinkedList = new LinkedList<Integer>();
        expectedLinkedList.add(7);
        expectedLinkedList.add(0);
        expectedLinkedList.add(8);
        LinkedList<Integer> l1 = new LinkedList<Integer>();
        l1.add(2);
        l1.add(4);
        l1.add(3);
        LinkedList<Integer> l2 = new LinkedList<Integer>();
        l2.add(5);
        l2.add(6);
        l2.add(4);
        Assert.assertEquals(expectedLinkedList, AddTwoNumbersAsALinkedList.getTwoNumberAsLinkedListSummed(l1, l2));
    }

    @Test
    public void testWithAListBiggerThanTheOther() {
        LinkedList<Integer> expectedLinkedList = new LinkedList<Integer>();
        expectedLinkedList.add(8);
        expectedLinkedList.add(1);
        expectedLinkedList.add(1);
        expectedLinkedList.add(1);
        LinkedList<Integer> l1 = new LinkedList<Integer>();
        l1.add(7);
        l1.add(0);
        l1.add(0);
        LinkedList<Integer> l2 = new LinkedList<Integer>();
        l2.add(1);
        l2.add(1);
        l2.add(1);
        l2.add(1);
        Assert.assertEquals(expectedLinkedList, AddTwoNumbersAsALinkedList.getTwoNumberAsLinkedListSummed(l1, l2));
    }
}