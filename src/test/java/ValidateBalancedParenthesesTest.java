import org.junit.Assert;
import org.junit.Test;

public class ValidateBalancedParenthesesTest {

    @Test
    public void test1(){
        Assert.assertTrue(ValidateBalancedParentheses.areParenthesesBalanced("((()))"));
    }

    @Test
    public void test2(){
        Assert.assertTrue(ValidateBalancedParentheses.areParenthesesBalanced("[()]{}"));
    }

    @Test
    public void test3(){
        Assert.assertFalse(ValidateBalancedParentheses.areParenthesesBalanced("({[)]"));
    }

    @Test
    public void test4(){
        Assert.assertTrue(ValidateBalancedParentheses.areParenthesesBalanced("{{(){[](((())))}}}"));
    }

    @Test
    public void test5(){
        Assert.assertTrue(ValidateBalancedParentheses.areParenthesesBalanced(""));
    }
}