import org.junit.Assert;
import org.junit.Test;

public class LongestPalindromicSubstringTest {

    @Test
    public void test() {
        Assert.assertEquals("anana", LongestPalindromicSubstring.getLongestPalindromicSubstring("banana"));
        Assert.assertEquals("illi", LongestPalindromicSubstring.getLongestPalindromicSubstring("million"));
        Assert.assertEquals("keepeek", LongestPalindromicSubstring.getLongestPalindromicSubstring("keepeek"));
        Assert.assertEquals("keepeekeepeek", LongestPalindromicSubstring.getLongestPalindromicSubstring("keepeekeepeek"));
    }
}