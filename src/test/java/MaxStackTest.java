import org.junit.Assert;
import org.junit.Test;

public class MaxStackTest {

    @Test
    public void test() {
        MaxStack maxStack = new MaxStack();
        maxStack.push(1);
        maxStack.push(2);
        maxStack.push(3);
        maxStack.push(2);
        Assert.assertEquals(Integer.valueOf(3), maxStack.getMax());
        maxStack.pop();
        maxStack.pop();
        Assert.assertEquals(Integer.valueOf(2), maxStack.getMax());
    }
}