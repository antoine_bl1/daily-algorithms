import model.Node;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Test;

public class FloorAndCeilingOfABSTTest {

    /*
               8
            /     \
           4       12
          / \     /  \
         2   6   10   14

     */
    @Test
    public void test(){
        Node<Integer> root = new Node<Integer>(8);
        root.setLeft(new Node<Integer>(4));
        root.setRight(new Node<Integer>(12));
        root.getLeft().setLeft(new Node<Integer>(2));
        root.getLeft().setRight(new Node<Integer>(6));
        root.getRight().setLeft(new Node<Integer>(10));
        root.getRight().setRight(new Node<Integer>(14));
        Pair<Integer, Integer> expectedFloorAndCeiling = Pair.of(4, 6);
        Assert.assertEquals(expectedFloorAndCeiling, FloorAndCeilingOfABST.getFloorAndCeiling(root, 5));
    }
}